<?php
/**
 * Code source de la classe PostgresAlterSchemaShell.
 *
 * PHP 5.3
 *
 * @package Postgres
 * @subpackage Console.Command
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */
// @codeCoverageIgnoreStart
App::uses( 'PostgresAbstractShell', 'Postgres.Console/Command' );
// @codeCoverageIgnoreEnd

/**
 * La classe PostgresAlterSchemaShell fournit des méthodes de modification de
 * base de données PostgreSQL.
  *
 * @package Postgres
 * @subpackage Console.Command
 */
class PostgresAlterSchemaShell extends PostgresAbstractShell
{
    /**
     * Description courte du shell
     *
     * @var string
     */
    public $description = 'Shell de modification de schéma de base de données PostgreSQL';

    /**
     * Liste des sous-commandes et de leur description.
     *
     * @var array
     */
    public $commands = array(
        'sequences' => array(
            'help' => 'Transformation des séquences de type SERIAL en IDENTITY (disponibles depuis PostgreSQL 10)'
        ),
    );

    /**
     * Liste des options et de leur description.
     *
     * @var array
     */
    public $options = array(
        'connection' => array(
            'short' => 'c',
            'help' => 'Le nom de la connection à la base de données',
            'default' => 'default',
        ),
        'force' => array(
            'short' => 'f',
            'help' => 'Ne pas poser de question pour les modifications de schéma de base de données',
            'boolean' => true
        ),
    );

    /**
     * Transformation des séquences de type SERIAL en IDENTITY (disponibles depuis PostgreSQL 10).
     *
     * @see https://stackoverflow.com/questions/59232753/how-to-change-a-table-id-from-serial-to-identity
     */
    public function sequences() {
        $this->out( date( 'H:i:s' )." - {$this->commands[__FUNCTION__]['help']} (sequences)" );

        if (!$this->params['force']) {
            $msgstr = 'Voulez-vous transformer l\'ensemble des séquences de type SERIAL en IDENTITY ?';
            $yes = mb_strtolower($this->in($msgstr, ['o', 'n'], 'n'));
            if ($yes === 'n') {
                $msgstr = 'Abandon de la transformation de l\'ensemble des séquences de type SERIAL en IDENTITY';
                $this->err("<warning>Avertissement</warning> {$msgstr}");
                $this->_stop(static::SUCCESS);
            }
        }

        $success = ( $this->Dbo->begin() !== false );

        $sql = "SELECT table_schema AS \"Model__schema\",
                        table_name AS \"Model__table\",
						column_name AS \"Model__column\",
						column_default AS \"Model__sequence\"
						FROM information_schema.columns
						WHERE column_default LIKE 'nextval(%::regclass)'
						ORDER BY table_schema, table_name, column_name";

        foreach( $this->Dbo->query( $sql ) as $model ) {
            $sql = [];

            $sql[] = sprintf(
                'ALTER TABLE "%s"."%s" ALTER "id" DROP DEFAULT;',
                $model['Model']['schema'],
                $model['Model']['table']
            );

            $sequence = preg_replace('/^nextval\(\'(.*)\'.*\)$/', '\1', $model['Model']['sequence']);
            $sql[] = sprintf('DROP SEQUENCE "%s";', $sequence);

            $sql[] = sprintf(
                'ALTER TABLE "%s"."%s" ALTER "%s" ADD GENERATED BY DEFAULT AS IDENTITY;',
                $model['Model']['schema'],
                $model['Model']['table'],
                $model['Model']['column']
            );

            $sql[] = sprintf(
                'SELECT SETVAL(pg_get_serial_sequence(\'%s.%s\', \'%s\'), (SELECT COALESCE(MAX(%s), 0) + 1 FROM "%s"."%s"), false);',
                $model['Model']['schema'],
                $model['Model']['table'],
                $model['Model']['column'],
                $model['Model']['column'],
                $model['Model']['schema'],
                $model['Model']['table'],
            );

            $format = 'Transformation de la séquence SERIAL en IDENTITY pour la table "%s"."%s"';
            $this->out(sprintf($format, $model['Model']['schema'], $model['Model']['table']), 1, static::VERBOSE);
            $success = $success && ($this->Dbo->query(implode("\n", $sql)) !== false);
        }

        if( $success ) {
            $success = ( $this->Dbo->commit() !== false ) && $success;
        }
        else {
            $success = ( $this->Dbo->rollback() !== false ) && $success;
        }

        if( $this->command === __FUNCTION__ ) {
            $this->_stop( $success ? self::SUCCESS : self::ERROR );
            return;
        }

        return $success;
    }
}
?>

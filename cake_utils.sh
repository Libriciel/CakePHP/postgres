#!/bin/sh
set -o errexit
set -o nounset

__FILE__=$0
__SCRIPT__=`basename $__FILE__`
__DIR__=`dirname "$0"`

PLUGIN_NAME="Postgres"
DIR_ROOT="${PWD}"
DIR_OUT="${DIR_ROOT}/out/Plugin_${PLUGIN_NAME}"

cake2_dir() {
    candidates="lib/Cake \
        vendor/cakephp/cakephp/lib/Cake \
        vendors/cakephp/cakephp/lib/Cake"

    for candidate in $candidates ; do
        if [ -d "${candidate}" ] ; then
            echo "${candidate}"
            return 0
        fi
    done

    echo ""
    >&2 echo "Could not find CakePHP's VERSION.txt file (searched in ${candidates})"
    return 1
}

cake2_check() {
    echo "Checking for a CakePHP 2.x install"

    fileName="`cake2_dir`/VERSION.txt"

    if [ -z "${fileName}" ] ; then
        >&2 echo "Could not find CakePHP's VERSION.txt file"
        return 1
    else
        grep --color=none "^2\.[0-9]\+" $fileName
        return $?
    fi
}

# Clears temporary files in app/tmp for a CakePHP 2.x install
cake2_clear() {
	cake2_check && \
		echo "Clearing the CakePHP 2.x install" && \
		sudo bash -c "( find app/tmp -type f ! -name 'empty' -exec rm {} \; )"

	return $?
}


usage()
{
    printf "NAME\n"
    printf "  %s\n" "$__SCRIPT__"
    printf "\nDESCRIPTION\n"
    printf "  Commandes courantes pour un projet CakePHP 2.x chez Libriciel SCOP\"\n"
    printf "\nSYNOPSIS\n"
    printf "  %s [OPTION] [COMMAND]\n" "$__SCRIPT__"
    printf "\nCOMMANDS\n"
    printf "  check\tVérifie que l'on soit bien à la racine d'un projet CakePHP 2.x\n"
    printf "  clear\tVérifie l'installation (commande check) et nettoie le cache d'un projet CakePHP 2.x\n"
    printf "  tail\tTail les fichiers de log de CakePHP 2.x (et les crée si besoin)\n"
    printf "  tests\tEffectue les tests unitaires du plugin ${PLUGIN_NAME}\n"
    printf "\nOPTIONS\n"
    printf "  -h\tAffiche cette aide\n"
    printf "\nEXEMPLES\n"
    printf "  %s -h\n" "$__SCRIPT__"
    printf "  %s clear\n" "$__SCRIPT__"
    printf "  %s tests\n" "$__SCRIPT__"
}

tests() {
    cake2_clear \
    && rm -rf "${DIR_OUT}/build" \
    && mkdir -p "${DIR_OUT}/build" \
    && "`cake2_dir`/Console/cake" \
        test ${PLUGIN_NAME} All${PLUGIN_NAME}Tests \
        --verbose \
        --strict \
        --stderr \
        --debug \
        --configuration "${__DIR__}/phpunit.xml" \
        --coverage-html "${DIR_OUT}/build/coverage" \
        --log-junit "${DIR_OUT}/build/phpunit.xml" \
        --coverage-clover "${DIR_OUT}/build/phpunit.coverage.xml"
}

# Create and tail the log files of a CakePHP 2.x install
cake2_tail() {
	local levels="emergency alert critical error warning notice info debug"

	cake2_check && \
		echo "Preparing to tail logs..."

	# @todo filter by user/group (apache|www-data|jenkins)
	for level in $levels ; do
		local file="app/tmp/logs/${level}.log"
		sudo touch "${file}"
		sudo chown www-data: "${file}"
	done
	tail -f app/tmp/logs/*.log

	return $?
}


(
    opts=`getopt --longoptions help -- h "$@"` \
        || ( >&2 usage ; exit 1 )
    eval set -- "$opts"
    while true ; do
        case "$1" in
            -h|--help)
                usage
                exit 0
            ;;
            --)
                shift
                break
            ;;
            *)
                >&2 usage
                exit 1
            ;;
        esac
    done

    case "${1:-}" in
        bake)
            shift
            bake "$@"
            exit $?
        ;;
        check)
            cake2_check
            exit $?
        ;;
        clear)
            cake2_clear
            exit $?
        ;;
        tail)
            cake2_tail
            exit $?
        ;;
        tests)
            tests
            exit $?
        ;;
        --)
            shift
            break
        ;;
        *)
            >&2 usage
            exit 1
        ;;
    esac

    exit 0
)
